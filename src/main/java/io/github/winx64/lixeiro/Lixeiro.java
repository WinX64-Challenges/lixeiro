package io.github.winx64.lixeiro;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.java.JavaPlugin;

public final class Lixeiro extends JavaPlugin implements Listener {

	private static final String TRASH_BIN_TITLE = ChatColor.DARK_RED + "" + ChatColor.BOLD + "Lixeiro";
	private static final String NO_CONSOLE = ChatColor.RED + "Esse comando pode ser usado apenas por jogadores!";

	@Override
	public void onEnable() {
		Bukkit.getPluginManager().registerEvents(this, this);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(NO_CONSOLE);
			return true;
		}

		((Player) sender).openInventory(createTrashInventory());
		return true;
	}

	@EventHandler
	public void onSignInteract(PlayerInteractEvent event) {
		if (event.getAction() != Action.RIGHT_CLICK_BLOCK) {
			return;
		}

		Block block = event.getClickedBlock();
		if (block.getType() != Material.SIGN_POST && block.getType() != Material.WALL_SIGN) {
			return;
		}

		Sign sign = (Sign) block.getState();
		if (!"[Lixo]".equals(sign.getLine(0))) {
			return;
		}

		event.getPlayer().openInventory(createTrashInventory());
	}

	private Inventory createTrashInventory() {
		return Bukkit.createInventory(null, 54, TRASH_BIN_TITLE);
	}
}
